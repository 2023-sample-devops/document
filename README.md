# General documents

There are a few things I have to cover first.

- I tested many times with my private repositories before committing the final result to you. So when you look at the [terraform](https://gitlab.com/2023-sample-devops/terraform) and [laravel](https://gitlab.com/2023-sample-devops/laravel) repo you only see a few commits. But the reality is that I had to edit a lot before it was complete. I wanted to give you the complete result without it being littered with commits.

![fact-commit](./images/fact-commit.jpg)

- When talking about AWS Well-Architected, one thing I want to emphasize about it is continuous monitoring and improvement. We did not have a strong architecture from the start and we only got it after many improvements.

- Security and cost are two issues that always go against each other in many projects that I have worked on. To balance these two issues, it takes a lot of time and adaptation from the company, the product, and the team. It is not simply some solution on the market.

## 1. Recommended system architecture

During the demo process, I almost built the architecture below. But I won't go into too much detail about how I built all those components, because it's beyond the scope of this demo. I just wanted to show you how I did it.

![system-architecture](./images/system-architecture.jpg)

## 2. How does AWS Well-Architected meet?

I will summarize the five main pillars and how each of these pillars can be met by this system.

### 2.1 Operational Excellence

- `Automation`: I automated both the application and the infrastructure. How specifically?
    - Automate testing, scanning, building and pushing docker images to ECR using Gitlab CI
    - Automate Terraform code with Gitlab CI. This is very important, it keeps the TF code consistent. Restrict the permissions of unnecessary IAM users/DevOps, limit users applying TF from local environments and no one controls.
    - Automatically deploy applications to EKS clusters using Helm and ArgoCD. You can go to the [argocd-apps](./argocd-apps/) folder to see the code I use. Of course, to apply it to the system, it will require some more operations that I cannot present all here.

- `Monitoring and alerts`: Although it is not shown in the code through the architecture I used above, we will have a logging system based on the EFK stack and a monitoring system based on Prometheus & Grafana. It will ensure continuous monitoring and alerts when there are problems.

### 2.2 Security

Implementing security solutions will take time and money. Within the limits of this demo as well as a basic system that can be guaranteed to be safe, I will present it in an easy-to-understand way from the outer layer to the inner layer. I have implemented:

- Use WAF (but I don't show it in Terraform because it's out of scope). Using WAF for some organizations is very expensive, but we can use it more economically. Specifically, instead of creating rules for all access requests to the application, we only create rules for the admin login section and important components. So at least we have an extra layer of assurance for the admin parts.

- Build all important resources in a private subnet and manage with Security Group. This helps protect against hackers scanning servers from the internet.

- Minimize intervention from other IAM users, using automation from Gitlab CI.

- Use SIEM Wazuh. This is a very effective open source solution for detecting attacks on the system. To make it, there will be basically 2 parts.
    - First, we build our own AMI with wazuh agent pre-installed in it. EKS cluster will use this AMI and every time a node is created, it will automatically connect to the master server. You can see the file [packer/ami-wazuh.json](./packer/ami-wazuh.json) to understand more, I use Packer to create AMI.
    - The second thing is that in a separate VPC that I call VPC Operation, we install the Wazuh master server. And of course I cannot explain how to install the Wazuh system or how to operate it here.

- Apply SAST, Secret Detection, Trivy Scanning to detect application source code problems.

Basically, we have enough layers of protection here:

- Communication layer with internet users
- Infrastructure layer
- Application layer (code)

Besides, to add an additional layer of internal security (ie to control access to the system from the internal team), in addition to IAM, I created an additional OpenVPN server. Connect via VPN to VPC Admin, and from here you can access other resources according to previously established policies.

### 2.3 Reliability

The best context we have is multi cloud and multi region. However, not every organization has the conditions to do that.

Within this demo, I only stop at 3 points:

- Auto scale for EKS cluster across multiple zones.
- Auto scale for pods through settings in Helm.
- Auto scale RDS storage, use multi az and automatically backup. Ensures recovery in case of errors.

### 2.4 Cost Optimization

With the hypothetical system, I did 4 things related to cost:

- Create minimal resources with a small size instead of using the default style (the rule is to use small and gradually upgrade if needed)
- Automate resource creation with Terraform. Minimize redundant resources due to errors.
- Use spot instance for EKS cluster, where possible, using Reserved Instances would be a better choice.
- Tagging resources, this is important if we later want to calculate costs based on applications/systems.

### 2.5 Performance Efficiency

Basically, the system is built on AWS services so there won't be too much to say here about it.